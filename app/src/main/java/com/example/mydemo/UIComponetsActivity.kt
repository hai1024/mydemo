package com.example.mydemo

import android.app.AlertDialog
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.mylibrary.MyDialog
import com.example.mylibrary.MyUtils
import com.facebook.soloader.ApkSoSource
import kotlinx.android.synthetic.main.activity_main.*


class UIComponetsActivity : BaseActivity(), View.OnClickListener {
    lateinit var dialog: Dialog
    lateinit var  blurBgDialog:Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Utils.mCurrentA = this
        setContentView(R.layout.activity_main)
        var a = ApkSoSource(this)
        print(a)
        print(MyUtils().getVersion())
//        var has=ContextCompat.checkSelfPermission(this,"android.permission.SYSTEM_ALERT_WINDOW")
//        if (has == PackageManager.PERMISSION_DENIED){
//
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                requestPermissions(listOf<String>("android.permission.SYSTEM_ALERT_WINDOW").toTypedArray(),1)
//            }
//        }
        bt1.setOnClickListener(this)
        bt2.setOnClickListener(this)
        bt3.setOnClickListener(this)
        bt4.setOnClickListener(this)
        bt5.setOnClickListener(this)
        bt6.setOnClickListener(this)
        bt0.setOnClickListener(this)


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Toast.makeText(this,"權限結果 " + grantResults[0],Toast.LENGTH_LONG).show()
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.bt1 -> {
//                MyDialog(this).apply {
//                    this.setTitle("测试")
//                    this.setMessage("测试")
//                }.show()

                val builder = AlertDialog.Builder(
                    applicationContext
                )
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder.setView(R.layout.dialog_content_view)
                }
                val alertDialog = builder.create()

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
                    alertDialog.window!!.setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT)
                } else {
                    alertDialog.window!!.setType(WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY)
                }

//                val view: View = alertDialog.layoutInflater.inflate(布局资源文件, null)
//                alertDialog.setView(view, 0, 0, 0, 0)
                alertDialog.show()
            }
            R.id.bt2 -> {
                var dialog= MyDialog(this).apply {
                    this.setTitle("测试")
                    this.setMessage("测试")
                }
                Thread {
                    Looper.prepare()
                    dialog.show()
                    Looper.loop()
                }.start()

            }
            R.id.bt3 -> {
                Thread {
                    Looper.prepare()
                    MyDialog(this).apply {
                        this.setTitle("测试")
                        this.setMessage("测试")
                    }.show()
                    Looper.loop()
                }.start()
            }
            R.id.bt4 ->{
                var dialog=AlertDialog.Builder(this).create()
                var view=View.inflate(this,R.layout.dialog_content_view,null)
                dialog.setView(view,20,20,0,0)
                dialog.window!!.setBackgroundDrawableResource(R.color.black)
                dialog.show()
            }
            R.id.bt5->{
                blurBgDialog = BlurBgDialog(this)
                blurBgDialog.run {
                    setOwnerActivity(this@UIComponetsActivity)
                    show()
                }
            }
            R.id.bt6->{
                blurBgDialog.window!!.decorView.setBackgroundColor(Color.RED)
            }
            R.id.bt0->{
                startActivity(Intent(this,BActivity::class.java))
            }
        }
    }
}