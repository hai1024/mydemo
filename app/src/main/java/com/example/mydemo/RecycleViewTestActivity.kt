package com.example.mydemo

import android.os.Bundle
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.Adapter
import kotlinx.android.synthetic.main.activity_test_recycle.*

class RecycleViewTestActivity():BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_recycle)

        rv?.apply {
            layoutManager = LinearLayoutManager(this@RecycleViewTestActivity,LinearLayoutManager.VERTICAL,false)
            adapter = object: Adapter<RecyclerView.ViewHolder>() {
                override fun onCreateViewHolder(
                    parent: ViewGroup,
                    viewType: Int
                ): RecyclerView.ViewHolder {

                    return object :RecyclerView.ViewHolder(TextView(this@RecycleViewTestActivity)){

                    }
                }

                override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
                    (holder.itemView as TextView).text = "这是 position $position"
                }

                override fun getItemCount(): Int {
                    return 10
                }
            }
        }
    }
}