package com.example.mydemo

import android.os.Bundle
import android.text.InputType
import android.view.View
import kotlinx.android.synthetic.main.activity_test_edittext.*

class EditTextViewTestActivity:BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test_edittext)

    }

    fun changeXjd(view: View) {
        et.inputType = InputType.TYPE_CLASS_NUMBER

        et.editableText?.apply {
            clear()
            insert(0,"110.00")
        }
        et.isEnabled=true

    }

    fun changeSjd(view: View) {
        et.inputType = InputType.TYPE_CLASS_TEXT
        et.isEnabled=false

        et.editableText.apply {
            clear()
            insert(0,"市价单")
        }
    }
}