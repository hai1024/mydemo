package com.example.mydemo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
    }

    fun toChartA(view: View) {
        startActivity(Intent(this,ChartActivity::class.java))
    }
    fun toUIComA(view: View) {
        startActivity(Intent(this,UIComponetsActivity::class.java))
    }

    fun testHs(view: View) {
        Thread.sleep(2000)
    }

    fun testFont(view: View) {
        startActivity(Intent(this,FontTestActivity::class.java))
    }
}