package com.example.mydemo

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.ContentValues.TAG
import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.iv
import kotlinx.android.synthetic.main.base_dialog.*

class BlurBgDialog(val srContext: Context):AlertDialog(srContext,R.style.simple_dialog) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.base_dialog)
        Utils.mCurrentD=this
        if(srContext is Activity){
            (srContext as Activity).window.decorView.run {
                isDrawingCacheEnabled = true
                val cache=drawingCache
                window?.run {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        statusBarColor = Color.TRANSPARENT
                    }
                    setGravity(Gravity.CENTER)
                    addFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
                    clearFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
                    decorView?.run {
                        background = BitmapDrawable(Utils.makeBlur(cache,context))
                        setGravity(Gravity.CENTER)
                        setPadding(0,0,0,0)

                    }
                }

            }

        }
        tv.setOnClickListener{
            this@BlurBgDialog.window!!.decorView.setBackgroundColor(Color.RED)

            val layoutParams = this@BlurBgDialog.window!!.decorView.layoutParams
            tvContent.text = "${layoutParams.height} ${layoutParams.width}"
            layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT
            layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
            this@BlurBgDialog.window!!.decorView.layoutParams = layoutParams

            this@BlurBgDialog.window!!.decorView.requestLayout()

            this@BlurBgDialog.window!!.decorView.parent.requestLayout()
            this@BlurBgDialog.onWindowAttributesChanged(layoutParams as WindowManager.LayoutParams?)

            window?.decorView?.viewTreeObserver?.addOnGlobalLayoutListener {
                val outRect = Rect()
                window?.decorView?.run {
                    getWindowVisibleDisplayFrame(outRect)
                    val contentRect = Rect();
                    dialog_content.getLocalVisibleRect(contentRect)
                    Log.d(TAG, "onCreate: " + contentRect)
                    if(Math.abs(height - outRect.bottom)>100 ){
                        Toast.makeText(context,"有软件键盘",Toast.LENGTH_LONG).show()
                    }
                }

            }
        }

    }

}