package com.example.mydemo

import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Typeface
import android.os.Bundle
import android.util.Log
import android.view.View
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.components.*
import com.github.mikephil.charting.components.YAxis.YAxisLabelPosition
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.IAxisValueFormatter
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import kotlinx.android.synthetic.main.activity_chart.*


class ChartActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chart)
    }
    fun showhodlePieChart() {
        // 设置每份所占数量
        var yvals = mutableListOf<PieEntry>(PieEntry(2.0f, "汉族"),
            PieEntry(3.0f, "回族"),
            PieEntry(4.0f, "壮族"),
            PieEntry(5.0f, "维吾尔族"),
            PieEntry(6.0f, "土家族")
        );
        //设置每份的颜色
        var colors = ArrayList<Int>();
        colors.add(Color.parseColor("#6785f2"));
        colors.add(Color.parseColor("#675cf2"));
        colors.add(Color.parseColor("#496cef"));
        colors.add(Color.parseColor("#aa63fa"));
        colors.add(Color.parseColor("#58a9f5"));

        val dataSet = PieDataSet(yvals, "Label")

        dataSet.setColors(colors);
//        dataSet.setValueL
        dataSet.setDrawValues(true);
//      当值位置为外边线时，表示线的前半段长度。
        dataSet.setValueLinePart1Length(0.4f);
//      当值位置为外边线时，表示线的后半段长度。
        dataSet.setValueLinePart2Length(0.4f);
//      当ValuePosits为OutsiDice时，指示偏移为切片大小的百分比
        dataSet.setValueLinePart1OffsetPercentage(50f);
        // 当值位置为外边线时，表示线的颜色。
        dataSet.valueLineColor= Color.RED
//        设置Y值的位置是在圆内还是圆外
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE)
//        设置Y轴描述线和填充区域的颜色一致
//        dataSet.setUsingSliceColorAsValueLineColor(false);
        dataSet.valueLineWidth=1f
//        设置每条之前的间隙
        dataSet.setSliceSpace(1f);

        //设置饼状Item被选中时变化的距离
        dataSet.setSelectionShift(15f);

        val data = PieData(dataSet)
//        data.setValueFormatter(PercentFormatter())
        data.setValueTextSize(12f);
        pieChart.data = data;
        pieChart.invalidate();
//        去除右下角lable
        val description = Description()
        description.setText("")
        pieChart.setDescription(description)
        //设置实心
        pieChart.run {
//            holeRadius=0.0f
            transparentCircleRadius=0.0f
            setDrawEntryLabels(false)
            getDescription().setEnabled(false); //取消右下角描述
            setUsePercentValues(true);//显示成百分比

            //获取图例
            var legend = getLegend();
            legend.isEnabled=false
            legend.setOrientation(Legend.LegendOrientation.VERTICAL);  //设置图例水平显示
            legend.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP); //顶部
            legend.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT); //右对其

            legend.setXEntrySpace(7f);//x轴的间距
            legend.setYEntrySpace(10f); //y轴的间距
            legend.setYOffset(10f);  //图例的y偏移量
            legend.setXOffset(10f);  //图例x的偏移量
            legend.setTextColor(Color.parseColor("#a1a1a1")); //图例文字的颜色
            legend.setTextSize(13f);  //图例文字的大小

        }

    }

    fun showChart(view: View) {
        showhodlePieChart()
    }


    private fun initPieChart(pieChart: PieChart) {
        pieChart.isDrawHoleEnabled = true
        pieChart.holeRadius = 70f
        pieChart.setHoleColor(Color.YELLOW)
        pieChart.centerText = "今日\n资金"
        pieChart.setCenterTextColor(Color.GRAY)
        pieChart.transparentCircleRadius = 40f
        pieChart.setTransparentCircleColor(Color.YELLOW)
        pieChart.setTransparentCircleAlpha(255)
        pieChart.description = null
        pieChart.setUsePercentValues(true)
        pieChart.setExtraOffsets(10f, 10f, 10f, 10f) //布局设置的 padding 无效,只能通过这个设置
        pieChart.setDrawEntryLabels(false) //x轴文字
        pieChart.minOffset = 0f
        val legend = pieChart.legend
        legend.isEnabled = false


        val mainInflow: Float = 3f //主力流入
        val mainOutflow: Float = 3f //主力流出
        val retailInflow: Float = 3f //散户流入
        val retailOutflow: Float = 5f //散户流出

        val xVals: List<String> = mutableListOf("主力流入","主力流出","散户流出","散户流入")
        val yVals: List<PieEntry> = mutableListOf(PieEntry(mainInflow, 0),PieEntry(mainOutflow, 1),PieEntry(retailOutflow, 2),PieEntry(retailInflow, 3))

//设置饼图数据

        //设置饼图数据
        val pieDataSet = PieDataSet(yVals, "")
            pieDataSet.setColors(
                *intArrayOf(
                    Color.RED,
                    Color.GREEN,
                    Color.DKGRAY,
                    Color.RED
                )
            )
        pieDataSet.sliceSpace = 0f
        pieDataSet.selectionShift = 6f
        pieDataSet.isHighlightEnabled = true
        pieDataSet.setDrawValues(false)
        //设置折线
        //        pieDataSet.setValueLineColor(getColor(R.color.white));
        //        pieDataSet.setValueLinePart1OffsetPercentage(80.f);
        //        pieDataSet.setValueLinePart1Length(0.8f);
        //        pieDataSet.setValueLinePart2Length(0.5f);
        //        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        //设置折线
        //        pieDataSet.setValueLineColor(getColor(R.color.white));
        //        pieDataSet.setValueLinePart1OffsetPercentage(80.f);
        //        pieDataSet.setValueLinePart1Length(0.8f);
        //        pieDataSet.setValueLinePart2Length(0.5f);
        //        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        val pieData = PieData(pieDataSet)
        pieData.setValueTextSize(12f)
        pieData.setValueFormatter(PercentFormatter())
        pieData.setValueTextColor(Color.WHITE)
        pieData.isHighlightEnabled = true
        pieChart.setUsePercentValues(true)

        pieChart.data = pieData
        pieChart.invalidate()
    }

    fun showChart2(view: View) {
        val yVals = mutableListOf<PieEntry>()
        yVals.add(PieEntry(5f,"lable5f"))
        yVals.add(PieEntry(6f,"lable6f"))
        yVals.add(PieEntry(10f,"lable10f"))
        yVals.add(PieEntry(15f,resources.getDrawable(android.R.drawable.ic_dialog_map),"hahh"))

        val dataSet = PieDataSet(yVals,"dataSetLabel")
        dataSet.addEntryOrdered(PieEntry(10f,"lable 新增的"))

        dataSet.colors = mutableListOf(Color.DKGRAY,Color.GREEN,Color.BLUE,Color.CYAN,Color.YELLOW)
        dataSet.setValueTextColors(mutableListOf(Color.RED))

        dataSet.selectionShift=0f
//        dataSet.isHighlightEnabled = true

        dataSet.xValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        dataSet.yValuePosition = PieDataSet.ValuePosition.OUTSIDE_SLICE
        dataSet.valueLineColor =Color.BLUE   //线的颜色
        dataSet.isValueLineVariableLength=false  //第二部分是否可变长度
        dataSet.valueLinePart1OffsetPercentage = 102f //影响第0个点位置
        dataSet.valueLinePart1Length=.3f    // 第一部分线 影响第1个点（两个线链接点）位置 相当于lableRadius
        dataSet.valueLinePart2Length=1f

        dataSet.valueTypeface = Typeface.DEFAULT_BOLD
        dataSet.valueTextSize = 12f
        dataSet.setDrawValues(true)
        dataSet.setAutomaticallyDisableSliceSpacing(true)

        dataSet.form = Legend.LegendForm.CIRCLE
        dataSet.formSize =3f

        dataSet.iconsOffset = MPPointF(10f,10f)

        dataSet.valueFormatter = PercentFormatter()
        dataSet.sliceSpace = 4f //每一部分的间距

        val data = PieData(dataSet)
        pieChartFundFlow.setExtraOffsets(0f,0f,0f,0f)
        pieChartFundFlow.data = data

        pieChartFundFlow.setDrawEntryLabels(false)
        pieChartFundFlow.description.isEnabled =false
        pieChartFundFlow.setUsePercentValues(true)

        pieChartFundFlow.isDrawHoleEnabled = true; // 不显示饼图中间的空洞
        pieChartFundFlow.holeRadius = 80f
        pieChartFundFlow.setHoleColor(Color.YELLOW)
        // 半透明的圆心
        pieChartFundFlow.transparentCircleRadius = 0f
        pieChartFundFlow.minOffset=8.5f
        setLegend()
        pieChartFundFlow.invalidate()
        runOnUiThread{
            Log.d("pieChartFundFlow","pieChartFundFlow.width ${pieChartFundFlow.width} pieChartFundFlow.height ${pieChartFundFlow.height} pieChartFundFlow.radius ${pieChartFundFlow.radius} circleBox ${pieChartFundFlow.circleBox}")
        }
    }

    fun setLegend() {
        var legend = pieChartFundFlow.legend;
        legend.isEnabled =true
        legend.formSize = 15f; // 图例的图形大小
        legend.textSize = 15f; // 图例的文字大小

        legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT; // 显示的位置水平居中
        legend.verticalAlignment = Legend.LegendVerticalAlignment.BOTTOM
        legend.orientation= Legend.LegendOrientation.HORIZONTAL
        legend.direction= Legend.LegendDirection.LEFT_TO_RIGHT
        legend.maxSizePercent=0.50f
        legend.isWordWrapEnabled = true

        legend.setDrawInside(false); // 设置图例在图中
        legend.xOffset = 0f
        legend.yOffset = 0f// 设置图例在垂直方向的偏移量
    }

    fun showbarChart(view: View) {
//        var datas = mutableListOf<BarEntry>(BarEntry(1f,90f),
//            BarEntry(2f,-50f),BarEntry
//        (3f,20f),BarEntry(4f,60f),
//            BarEntry(5f,20f),BarEntry
//                (3f,20f))
        var datas = mutableListOf<BarEntry>(BarEntry(0f,-80f),BarEntry(1f,80f),BarEntry(2f,180f))

        var dataSet = BarDataSet(datas,"barLabel")
        dataSet.setDrawValues(true)
        dataSet.isHighlightEnabled = false
        var colors=mutableListOf<Int>()

        datas.forEach {
            colors.add(if (it.y>0) android.graphics.Color.GREEN else android.graphics.Color.YELLOW)
        }
        dataSet.colors= colors
        dataSet.formLineWidth = 5f
        dataSet.barBorderWidth = 0f
        dataSet.setValueTextColors(colors)
        dataSet.valueTextSize=20f
        dataSet.setDrawValues(true)
//        dataSet.formLineDashEffect = DashPathEffect()
        var barData=BarData(dataSet)

        barData.barWidth = .5f
        barChart.data = barData
        barChart.description.isEnabled = false
//        barChart.maxHighlightDistance = 0f

        barChart.setDrawGridBackground(true);
        barChart.setGridBackgroundColor(Color.RED)
        barChart.setDrawBorders(true)

        barChart.isDragEnabled = false
        barChart.setScaleEnabled(false)

        barChart.legend.isEnabled = false //不显示图例

//        barChart.axisLeft.isEnabled = false
        barChart.axisRight.isEnabled = false
        barChart.axisLeft.apply {
            isEnabled =true
            setPosition(YAxisLabelPosition.INSIDE_CHART)
            setDrawLabels(false)
        }
        barChart.xAxis.isEnabled = false
        barChart.xAxis.labelCount = 1
        barChart.setDrawValueAboveBar(true)

        barChart.axisLeft.apply {
            mAxisMaximum=dataSet.yMax
            mAxisMinimum = dataSet.yMin
            spaceTop=15f
            spaceBottom=15f
            setDrawAxisLine(false)
            setDrawZeroLine(false)
            setDrawGridLines(false)
            setDrawLimitLinesBehindData(true)
            addLimitLine(LimitLine(0f,"").apply {
                lineWidth = 2f
                lineColor = Color.BLUE
                enableDashedLine(10f,5f,10f) })
        }
        barChart.isAutoScaleMinMaxEnabled = true

//        barChart.setExtraOffsets(0f,0f,0f,0f)
        barChart.minOffset = 0f
        barChart.animateXY(200,200)
//        barChart.invalidate()
    }

    fun showLineChart(view: View) {
        val yVals = mutableListOf<Entry>(Entry(1f,200f),
            Entry(1f,100f), Entry
        (2f,400f), Entry
        (3f,700f), Entry
                (4f,500f)
        )
        val dataSet = LineDataSet(yVals,"资金流向").apply {
            setDrawValues(true)
            setDrawCircles(false)
            color = Color.GREEN
        }
        val data = LineData(dataSet).apply {
        }
        lineChart.data = data
        lineChart.apply {
            setPinchZoom(false)
            description.isEnabled = false
            legend.isEnabled=false
            setViewPortOffsets(0f,40f,0f,40f)
            minOffset = 10F
            xAxis.apply {
                setAvoidFirstLastClipping(true)
                position=XAxis.XAxisPosition.BOTTOM
                textSize = 15f
                setDrawGridLines(false)
                setAxisLineDashedLine(DashPathEffect(mutableListOf<Float>(5f,2f).toFloatArray(),10f))
                axisLineColor = Color.YELLOW
                valueFormatter = IAxisValueFormatter { value, _ -> "$value 时间" }
                isScaleYEnabled = false
                setPinchZoom(false)
                resetZoom()
                setLabelCount(3,true)
            }

            axisRight.isEnabled=false
            axisLeft.apply {
                setPinchZoom(false)
                setDrawZeroLine(true)
                setPosition(YAxisLabelPosition.INSIDE_CHART)
                setClipValuesToContent(true)
                textSize = 10F
                enableGridDashedLine(5f,2f,10f)
                gridColor = Color.YELLOW
                isScaleYEnabled = true
                setLabelCount(4,true)
                yOffset = -7f
            }
        }
        lineChart.setTouchEnabled(false)
        lineChart.animateXY(200,200)
    }

    fun showLineChart2(view: View) {
        lineChart2.apply {
            val value = mutableListOf<Entry>(Entry(101f,10f),Entry(200f,20f),Entry(300f,40f),Entry(4110f,10f))
            val localDataSet = LineDataSet(value,"dataset").apply {
                setDrawValues(false)
            }
            val localLineData = LineData(localDataSet)
            setScaleEnabled(false)

            description.isEnabled=false
            legend.isEnabled =false

            xAxis.apply {
                position=XAxis.XAxisPosition.BOTTOM
                setLabelCount(3,true)
                valueFormatter = IAxisValueFormatter { value, _ -> value.toInt().toString() }
                axisMinimum =100f
                setAvoidFirstLastClipping(true)
//                setenable
            }

            axisRight.isEnabled=false

            axisLeft.apply {
                setLabelCount(4,true)
                yOffset=-5f
                xOffset =10f
                setPosition(YAxisLabelPosition.INSIDE_CHART)
                axisMinimum = localDataSet.yMin
                axisMaximum = localDataSet.yMax

                addLimitLine(LimitLine(localDataSet.yMax-5,"35").apply {
                    labelPosition = LimitLine.LimitLabelPosition.LEFT_BOTTOM
                    yOffset=10f
                    xOffset=10f
                })
            }
            minOffset=0f
            setClipValuesToContent(true)

            data = localLineData
            notifyDataSetChanged()
            invalidate()
        }
    }

}