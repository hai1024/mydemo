package com.example.mydemo

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieDataSet.ValuePosition
import com.github.mikephil.charting.renderer.PieChartRenderer
import com.github.mikephil.charting.utils.ColorTemplate
import com.github.mikephil.charting.utils.MPPointF
import com.github.mikephil.charting.utils.Utils
import kotlin.math.sin

class MyPieChart(context: Context?, attrs: AttributeSet?, defStyle:Int):PieChart( context, attrs,  defStyle) {
    constructor(context: Context?, attrs: AttributeSet?):this(context,attrs,0)
    override fun init() {
        super.init()

        mRenderer = object :PieChartRenderer(this, mAnimator, mViewPortHandler){
            override fun drawValues(c: Canvas?) {
                val center = mChart.centerCircleBox
                // get whole the radius

                // get whole the radius
                val radius = mChart.radius
                val rotationAngle = mChart.rotationAngle
                val drawAngles = mChart.drawAngles
                val absoluteAngles = mChart.absoluteAngles

                val phaseX = mAnimator.phaseX
                val phaseY = mAnimator.phaseY

                val holeRadiusPercent = mChart.holeRadius / 100f
                var labelRadiusOffset = radius / 10f * 3.6f

                if (mChart.isDrawHoleEnabled) {
                    labelRadiusOffset = (radius - radius * holeRadiusPercent) / 2f
                }

                val labelRadius = radius - labelRadiusOffset

                val data = mChart.data
                val dataSets = data.dataSets

                val yValueSum = data.yValueSum

                val drawEntryLabels = mChart.isDrawEntryLabelsEnabled

                var angle: Float
                var xIndex = 0

                c!!.save()

                val offset = Utils.convertDpToPixel(5f)

                for (i in dataSets.indices) {
                    val dataSet = dataSets[i]
                    val drawValues = dataSet.isDrawValuesEnabled
                    if (!drawValues && !drawEntryLabels) continue
                    val xValuePosition = dataSet.xValuePosition
                    val yValuePosition = dataSet.yValuePosition

                    // apply the text-styling defined by the DataSet
                    applyValueTextStyle(dataSet)
                    val lineHeight = (Utils.calcTextHeight(mValuePaint, "Q")
                            + Utils.convertDpToPixel(4f))
                    val formatter = dataSet.valueFormatter
                    val entryCount = dataSet.entryCount
                    mValueLinePaint.strokeWidth = Utils.convertDpToPixel(dataSet.valueLineWidth)
                    val sliceSpace = getSliceSpace(dataSet)
                    val iconsOffset = MPPointF.getInstance(dataSet.iconsOffset)
                    iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x)
                    iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y)
                    for (j in 0 until entryCount) {
                        mValueLinePaint.color = dataSet.getColor(j)
                        val entry = dataSet.getEntryForIndex(j)
                        angle = if (xIndex == 0) 0f else absoluteAngles[xIndex - 1] * phaseX
                        val sliceAngle = drawAngles[xIndex]
                        val sliceSpaceMiddleAngle = sliceSpace / (Utils.FDEG2RAD * labelRadius)

                        // offset needed to center the drawn text in the slice
                        val angleOffset = (sliceAngle - sliceSpaceMiddleAngle / 2f) / 2f
                        angle += angleOffset
                        val transformedAngle = rotationAngle + angle * phaseY
                        val value: Float = if (mChart.isUsePercentValuesEnabled) entry.y / yValueSum * 100f else entry.getY()
                        val sliceXBase = Math.cos((transformedAngle * Utils.FDEG2RAD).toDouble())
                            .toFloat()
                        val sliceYBase = Math.sin((transformedAngle * Utils.FDEG2RAD).toDouble())
                            .toFloat()
                        val drawXOutside = drawEntryLabels &&
                                xValuePosition == ValuePosition.OUTSIDE_SLICE
                        val drawYOutside = drawValues &&
                                yValuePosition == ValuePosition.OUTSIDE_SLICE
                        val drawXInside = drawEntryLabels &&
                                xValuePosition == ValuePosition.INSIDE_SLICE
                        val drawYInside = drawValues &&
                                yValuePosition == ValuePosition.INSIDE_SLICE
                        if (drawXOutside || drawYOutside) {
                            val valueLineLength1 = dataSet.valueLinePart1Length
                            val valueLineLength2 = dataSet.valueLinePart2Length
                            val valueLinePart1OffsetPercentage =
                                dataSet.valueLinePart1OffsetPercentage / 100f
                            var pt2x: Float
                            var pt2y: Float
                            var labelPtx: Float
                            var labelPty: Float
                            var line1Radius: Float
                            line1Radius =
                                if (mChart.isDrawHoleEnabled) (radius - radius * holeRadiusPercent) * valueLinePart1OffsetPercentage +radius * holeRadiusPercent else radius * valueLinePart1OffsetPercentage
                            val polyline2Width =
                                if (dataSet.isValueLineVariableLength) labelRadius * valueLineLength2 * Math.abs(
                                    sin((transformedAngle * Utils.FDEG2RAD).toDouble())
                                )
                                    .toFloat() else labelRadius * valueLineLength2
                            val pt0x = line1Radius * sliceXBase + center.x
                            val pt0y = line1Radius * sliceYBase + center.y
                            val pt1x = labelRadius * (1 + valueLineLength1) * sliceXBase + center.x
                            val pt1y = labelRadius * (1 + valueLineLength1) * sliceYBase + center.y
                            if (transformedAngle % 360.0 >= 90.0 && transformedAngle % 360.0 <= 270.0) {
                                pt2x = pt1x - polyline2Width
                                pt2y = pt1y
                                mValuePaint.textAlign = Paint.Align.RIGHT
                                if (drawXOutside) paintEntryLabels.textAlign = Paint.Align.RIGHT
                                labelPtx = pt2x - offset
                                labelPty = pt2y
                            } else {
                                pt2x = pt1x + polyline2Width
                                pt2y = pt1y
                                mValuePaint.textAlign = Paint.Align.LEFT
                                if (drawXOutside) paintEntryLabels.textAlign = Paint.Align.LEFT
                                labelPtx = pt2x + offset
                                labelPty = pt2y
                            }
                            if (dataSet.valueLineColor != ColorTemplate.COLOR_NONE) {
                                c!!.drawLine(pt0x, pt0y, pt1x, pt1y, mValueLinePaint)
                                c!!.drawLine(pt1x, pt1y, pt2x, pt2y, mValueLinePaint)
                            }

                            // draw everything, depending on settings
                            if (drawXOutside && drawYOutside) {
                                drawValue(
                                    c,
                                    formatter,
                                    value,
                                    entry,
                                    0,
                                    labelPtx,
                                    labelPty,
                                    dataSet.getValueTextColor(j)
                                )
                                if (j < data.entryCount && entry.label != null) {
                                    drawEntryLabel(c, entry.label, labelPtx, labelPty + lineHeight)
                                }
                            } else if (drawXOutside) {
                                if (j < data.entryCount && entry.label != null) {
                                    drawEntryLabel(
                                        c,
                                        entry.label,
                                        labelPtx,
                                        labelPty + lineHeight / 2f
                                    )
                                }
                            } else if (drawYOutside) {
                                drawValue(
                                    c,
                                    formatter,
                                    value,
                                    entry,
                                    0,
                                    labelPtx,
                                    labelPty + lineHeight / 2f,
                                    dataSet
                                        .getValueTextColor(j)
                                )
                            }
                        }
                        if (drawXInside || drawYInside) {
                            // calculate the text position
                            val x = labelRadius * sliceXBase + center.x
                            val y = labelRadius * sliceYBase + center.y
                            mValuePaint.textAlign = Paint.Align.CENTER

                            // draw everything, depending on settings
                            if (drawXInside && drawYInside) {
                                drawValue(
                                    c,
                                    formatter,
                                    value,
                                    entry,
                                    0,
                                    x,
                                    y,
                                    dataSet.getValueTextColor(j)
                                )
                                if (j < data.entryCount && entry.label != null) {
                                    drawEntryLabel(c, entry.label, x, y + lineHeight)
                                }
                            } else if (drawXInside) {
                                if (j < data.entryCount && entry.label != null) {
                                    drawEntryLabel(c, entry.label, x, y + lineHeight / 2f)
                                }
                            } else if (drawYInside) {
                                drawValue(
                                    c,
                                    formatter,
                                    value,
                                    entry,
                                    0,
                                    x,
                                    y + lineHeight / 2f,
                                    dataSet.getValueTextColor(j)
                                )
                            }
                        }
                        if (entry.icon != null && dataSet.isDrawIconsEnabled) {
                            val icon = entry.icon
                            val x = (labelRadius + iconsOffset.y) * sliceXBase + center.x
                            var y = (labelRadius + iconsOffset.y) * sliceYBase + center.y
                            y += iconsOffset.x
                            Utils.drawImage(
                                c,
                                icon,
                                x.toInt(),
                                y.toInt(),
                                icon.intrinsicWidth,
                                icon.intrinsicHeight
                            )
                        }
                        xIndex++
                    }
                    MPPointF.recycleInstance(iconsOffset)
                }
                MPPointF.recycleInstance(center)
                c!!.restore()
            }
        }

    }
}