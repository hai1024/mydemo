package com.example.mydemo

import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.transition.*
import androidx.appcompat.app.AppCompatActivity

open class BaseActivity:AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val shareTransition = ChangeTransform().setDuration(500)
            val windowTransition=Slide().setDuration(500)

            window.run {
                sharedElementEnterTransition=shareTransition
                sharedElementExitTransition=shareTransition
//                sharedElementReenterTransition=shareTransition
//                sharedElementReturnTransition=shareTransition
                exitTransition=Fade().setDuration(500)
                enterTransition=windowTransition
//                reenterTransition=Slide().setDuration(4000)
//                returnTransition =windowTransition
            }
        };

    }

    override fun startActivity(intent: Intent?) {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val slideOut = Slide();
            slideOut.setDuration(500);
//设置从左边退出
            slideOut.setSlideEdge(android.view.Gravity.LEFT);
//设置为退出
            slideOut.setMode(Visibility.MODE_OUT);
            getWindow().setExitTransition(slideOut);
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())

//            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this,findViewById(R.id.title),"title").toBundle())
        }else{
            startActivity(intent)
        }
    }
}