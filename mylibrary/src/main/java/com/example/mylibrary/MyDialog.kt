package com.example.mylibrary

import android.app.Dialog
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.Gravity
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog


class MyDialog(context:Context): AlertDialog(context,R.style.custom_dialog){
    val TAG = "test"
    override fun create() {
        super.create()
//        window.decorView
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.let{
        Log.d(TAG, "Build.VERSION.SDK_INT " + Build.VERSION.SDK_INT)
               it.setType(WindowManager.LayoutParams.TYPE_APPLICATION)
        }

    }
}